/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenlazadas;

/**
 *
 * @author ameri
 */
public class Lista {

    protected Nodo inicio, fin;

    public Lista() {
        inicio = null;
        fin = null;
    }

    public void agregarInicio(String valor) {
        inicio = new Nodo(valor, inicio);
        if (fin == null) {
            fin = inicio;
        }
    }

    public void mostrarLista() {
        Nodo mostrar = inicio;
        System.out.println("");
        while (mostrar != null) {
            System.out.print("[" + mostrar.valor + "] -->");
            mostrar = mostrar.sig;
        }
        System.out.println("");
    }

    public String borrarInicio() {
        String valor = inicio.valor;
        if (inicio == fin) {
            inicio = null;
            fin = null;
        } else {
            inicio = inicio.sig;
        }
        return valor;
    }

    public boolean esVacia() {
        if (inicio == null) {
            return true;
        } else {
            return false;
        }
    }

    public void AgregarFinal(String valor) {
        if (!esVacia()) {
            fin.sig = new Nodo(valor);
            fin = fin.sig;
        } else {
            inicio = fin = new Nodo(valor);
        }
    }

    public String borrarFinal() {
        String valor = fin.valor;
        if (inicio == fin) {
            inicio = fin = null;
        } else {
            Nodo temporal = inicio;
            // Encontrar el final de la Lista
            while (temporal.sig != fin) {
                temporal = temporal.sig;
            }
            fin = temporal;
            fin.sig = null;
        }
        return valor;
    }
}
