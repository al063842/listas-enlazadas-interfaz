/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenlazadas;

import javax.swing.JOptionPane;

/**
 *
 * @author ameri
 */
public class ListasEnlazadas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Lista ListaGra = new Lista();

        int opcion = 0;
        do {

            try {
                opcion = Integer.parseInt(JOptionPane.showInputDialog(null, ""
                        + "1.- Añadir valor al inicio"
                        + "\n2.- Borrar valor al inicio"
                        + "\n3.- Añadir valor al final"
                        + "\n4.- Borrar valor al final"
                        + "\n5.- Mostrar lista"
                        + "\n6.- Salir"));

                switch (opcion) {
                    case 1:
                        String dato = JOptionPane.showInputDialog(null, "Ingrese el valor que desea añadir al inicio de la lista");
                        ListaGra.agregarInicio(dato);
                        break;
                    case 2:
                        JOptionPane.showInputDialog(null, "Usted borro el valor del inicio: " + ListaGra.borrarInicio());
                        break;
                    case 3:
                        dato = JOptionPane.showInputDialog(null, "Ingrese el valor que desea añadir al final de la lista");
                        ListaGra.AgregarFinal(dato);
                        break;
                    case 4:
                        JOptionPane.showInputDialog(null, "Usted borro el valor del final: " + ListaGra.borrarFinal());
                        break;
                    case 5:
                        System.out.println("La lista contiene estos datos");
                        ListaGra.mostrarLista();
                        break;
                    case 6:
                        System.out.println("Salir");
                        JOptionPane.showInputDialog(null, "Fin");
                        break;

                }
            } catch (NumberFormatException e) {
                System.out.println("Error Ingrese un entero: " + e.getMessage());
            }
        } while (opcion != 6);
    }
}
