/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenlazadas;

/**
 *
 * @author ameri
 */
public class Nodo {

    public String valor;
    public Nodo sig;

    public Nodo(String d) {
        this.valor = d;

        this.sig = null;
    }

    public Nodo(String d, Nodo n) {
        valor = d;
        sig = n;
    }
}
